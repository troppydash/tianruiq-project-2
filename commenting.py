import os, glob, re

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def getline(text, index):
    line = 1
    for i in range(index):
        if text[i] == '\n':
            line += 1

    lines = text.split('\n')

    return line, lines[line-1]

def analyze(text: str, filename: str):
    regex = r"(\/\*\*(\w|\s|\*|\@|,|<|>|\.|\-|\(|\))+\*\/\s*(@Override)?\s*public)"
    ranges = []
    for m in re.finditer(regex, text):
        ranges.append(m.span())

    for m in re.finditer(r"public", text):
        r = m.span()

        for z in ranges:
            if r[0] >= z[0] and r[1] <= z[1]:
                break
        else:
            n, line = getline(text, r[1])
            print(f'{bcolors.WARNING}{filename} at line {n}:{bcolors.ENDC}')
            print(f'    {line.strip()}')

def search(root: str):
    for filename in glob.glob(os.path.join(root, '**/*.java'), recursive=True):
        with open(filename, 'r') as f:
            analyze(f.read(), filename)
            # break




search('src')
