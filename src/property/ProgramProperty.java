package property;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Singleton class that contains the reference to various program properties
 */
public class ProgramProperty {
    /** contains the singleton properties */
    private static ProgramProperty instance = null;

    /** Game properties */
    public final Properties gameProperties;
    /** Message properties */
    public final Properties messageProperties;

    private ProgramProperty() {
        // loads the two property files
        this.gameProperties = readPropertiesFile("res/app.properties");
        this.messageProperties = readPropertiesFile("res/message_en.properties");
    }

    /**
     * Returns a string property from the message file
     *
     * @param name Property name
     * @return Property value
     */
    public String getMessageProperty(String name) {
        return this.messageProperties.getProperty(name);
    }

    /**
     * Returns a string property from the game file
     *
     * @param name Property name
     * @return Property value
     */
    public String getGameProperty(String name) {
        return this.gameProperties.getProperty(name);
    }

    /**
     * Returns an integer property from the game file
     *
     * @param name Property name
     * @return Property int value
     */
    public int getGamePropertyInt(String name) {
        return Integer.parseInt(this.gameProperties.getProperty(name));
    }

    /**
     * Returns a double property from the game file
     *
     * @param name Property name
     * @return Property double value
     */
    public double getGamePropertyDouble(String name) {
        return Double.parseDouble(this.gameProperties.getProperty(name));
    }

    /**
     * Returns a singleton instance of the properties
     *
     * @return The singleton
     */
    public static ProgramProperty getInstance() {
        if (instance == null) {
            instance = new ProgramProperty();
        }
        return instance;
    }


    /***
     * Method that reads a properties file and return a Properties object
     * @param configFile: the path to the properties file
     * @return Properties object
     */
    private static Properties readPropertiesFile(String configFile) {
        Properties appProps = new Properties();
        try {
            appProps.load(new FileInputStream(configFile));
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

        return appProps;
    }
}
