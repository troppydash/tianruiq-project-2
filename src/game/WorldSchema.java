package game;

import bagel.Image;
import bagel.util.Point;

import engine.Sprite;
import engine.SpriteBuilder;
import engine.components.colliders.PlatformCollider;
import engine.components.image.DynamicImage;
import engine.components.image.SimpleImage;
import engine.components.colliders.CircleCollider;
import engine.components.colliders.RectCollider;
import engine.components.CyclicalMovement;
import game.entities.*;
import property.ProgramProperty;

import java.util.List;
import java.util.Scanner;

/**
 * Class to parse the world files
 */
public class WorldSchema {
    // world references
    /**
     * Player reference
     */
    public static final String PLAYER = "PLAYER";
    /**
     * Scrollable reference
     */
    public static final String SCROLLABLE = "SCROLLABLE";
    /**
     * Boss reference
     */
    public static final String BOSS = "BOSS";

    // collider levels
    /**
     * Screen collider level
     */
    public static final int SCREEN_LEVEL = 0;
    /**
     * Enemy collider level
     */
    public static final int ENEMY_LEVEL = 1;
    /**
     * Coin collider level
     */
    public static final int COIN_LEVEL = 2;
    /**
     * Platform collider level
     */
    public static final int PLATFORM_LEVEL = 3;
    /**
     * Flag collider level
     */
    public static final int FLAG_LEVEL = 4;
    /**
     * Powerup collider level
     */
    public static final int POWERUP_LEVEL = 5;
    /**
     * Boss collider level
     */
    public static final int BOSS_LEVEL = 6;
    /**
     * Fireball collider level
     */
    public static final int FIREBALL_LEVEL = 7;

    /**
     * Create a world sprite given an input csv scanner
     *
     * @param scanner Scanner of a csv file
     * @return A world sprite
     */
    public static Sprite buildWorld(Scanner scanner) {
        ProgramProperty properties = ProgramProperty.getInstance();

        // create world
        SpriteBuilder world = new SpriteBuilder();

        // create scrollable
        SpriteBuilder scrollable = new SpriteBuilder();
        scrollable.setName(SCROLLABLE);
        world.addChild(scrollable);


        // create world collider
        double width = properties.getGamePropertyDouble("windowWidth");
        double height = properties.getGamePropertyDouble("windowHeight");
        SpriteBuilder screen = new SpriteBuilder(new Sprite(new Point(width / 2, height / 2)));
        screen.addComponent(new RectCollider(
                List.of(SCREEN_LEVEL),
                width,
                height
        ));
        world.addChild(screen);

        while (scanner.hasNextLine()) {
            // parse line
            String line = scanner.nextLine();
            String[] parts = line.split(",");
            assert parts.length == 3;

            String type = parts[0];
            int x = Integer.parseInt(parts[1]);
            int y = Integer.parseInt(parts[2]);

            switch (type) {
                case "PLAYER": {
                    // create player
                    SpriteBuilder player = new SpriteBuilder(new Player(
                            new Point(x, y),
                            properties.getGamePropertyDouble("gameObjects.player.radius"),
                            properties.getGamePropertyDouble("gameObjects.player.health"),
                            properties.getGamePropertyDouble("gameObjects.platform.speed")
                    ));
                    player.addComponent(new CircleCollider(
                            List.of(
                                    SCREEN_LEVEL, ENEMY_LEVEL, COIN_LEVEL,
                                    PLATFORM_LEVEL, FLAG_LEVEL, POWERUP_LEVEL,
                                    BOSS_LEVEL, FIREBALL_LEVEL
                            ),
                            properties.getGamePropertyDouble("gameObjects.player.radius")
                    ));
                    player.addComponent(new DynamicImage(
                            List.of(
                                    new Image(properties.getGameProperty("gameObjects.player.imageLeft")),
                                    new Image(properties.getGameProperty("gameObjects.player.imageRight"))
                            )
                    ));
                    player.setName(PLAYER);
                    world.addChild(player);
                    break;
                }
                case "PLATFORM": {
                    // create platform
                    Image platformImage = new Image(properties.getGameProperty("gameObjects.platform.image"));
                    SpriteBuilder platform = new SpriteBuilder(new Platform(new Point(x, y)));
                    platform.addComponent(new SimpleImage(platformImage, true));
                    platform.addComponent(new RectCollider(
                            List.of(PLATFORM_LEVEL),
                            platformImage.getWidth(),
                            platformImage.getHeight()
                    ));
                    scrollable.addChild(platform);
                    break;
                }
                case "ENEMY": {
                    // create enemy
                    SpriteBuilder enemy = new SpriteBuilder(new Enemy(
                            new Point(x, y),
                            properties.getGamePropertyDouble("gameObjects.enemy.damageSize")
                    ));
                    enemy.addComponent(new SimpleImage(
                            new Image(properties.getGameProperty("gameObjects.enemy.image")),
                            true
                    ));
                    enemy.addComponent(new CircleCollider(
                            List.of(ENEMY_LEVEL),
                            properties.getGamePropertyDouble("gameObjects.enemy.radius")
                    ));
                    enemy.addComponent(new CyclicalMovement(
                            properties.getGamePropertyDouble("gameObjects.enemy.maxRandomDisplacementX"),
                            properties.getGamePropertyDouble("gameObjects.enemy.randomSpeed")
                    ));
                    scrollable.addChild(enemy);
                    break;
                }
                case "COIN": {
                    // create coin
                    SpriteBuilder coin = new SpriteBuilder(new Coin(
                            new Point(x, y),
                            properties.getGamePropertyInt("gameObjects.coin.value")
                    ));
                    coin.addComponent(new SimpleImage(
                            new Image(properties.getGameProperty("gameObjects.coin.image")),
                            true
                    ));
                    coin.addComponent(new CircleCollider(
                            List.of(COIN_LEVEL),
                            properties.getGamePropertyDouble("gameObjects.coin.radius")
                    ));
                    scrollable.addChild(coin);
                    break;
                }
                case "END_FLAG": {
                    // create flag
                    SpriteBuilder flag = new SpriteBuilder(new Flag(new Point(x, y)));
                    flag.addComponent(new SimpleImage(
                            new Image(properties.getGameProperty("gameObjects.endFlag.image")),
                            true
                    ));
                    flag.addComponent(new CircleCollider(
                            List.of(FLAG_LEVEL),
                            properties.getGamePropertyDouble("gameObjects.endFlag.radius")
                    ));
                    scrollable.addChild(flag);
                    break;
                }
                case "FLYING_PLATFORM": {
                    // create platform
                    Image platformImage = new Image(properties.getGameProperty("gameObjects.flyingPlatform.image"));
                    SpriteBuilder platform = new SpriteBuilder(new FlyingPlatform(new Point(x, y)));
                    platform.addComponent(new SimpleImage(platformImage, true));
                    platform.addComponent(new PlatformCollider(
                            List.of(PLATFORM_LEVEL),
                            // we use full width in the platform
                            2 * properties.getGamePropertyDouble("gameObjects.flyingPlatform.halfLength"),
                            properties.getGamePropertyDouble("gameObjects.flyingPlatform.halfHeight")
                    ));
                    platform.addComponent(new CyclicalMovement(
                            properties.getGamePropertyDouble("gameObjects.flyingPlatform.maxRandomDisplacementX"),
                            properties.getGamePropertyDouble("gameObjects.flyingPlatform.randomSpeed")
                    ));
                    scrollable.addChild(platform);
                    break;
                }
                case "DOUBLE_SCORE": {
                    SpriteBuilder powerup = new SpriteBuilder(new Powerup(
                            new Point(x, y),
                            PowerType.DOUBLE_POINTS,
                            properties.getGamePropertyInt("gameObjects.doubleScore.maxFrames")
                    ));
                    Image image = new Image(properties.getGameProperty("gameObjects.doubleScore.image"));
                    powerup.addComponent(new SimpleImage(
                            image, true
                    ));
                    powerup.addComponent(new CircleCollider(
                            List.of(WorldSchema.POWERUP_LEVEL),
                            properties.getGamePropertyDouble("gameObjects.doubleScore.radius")
                    ));
                    scrollable.addChild(powerup);
                    break;
                }
                case "INVINCIBLE_POWER": {
                    SpriteBuilder powerup = new SpriteBuilder(new Powerup(
                            new Point(x, y),
                            PowerType.INVINCIBLE,
                            properties.getGamePropertyInt("gameObjects.invinciblePower.maxFrames")
                    ));
                    Image image = new Image(properties.getGameProperty("gameObjects.invinciblePower.image"));
                    powerup.addComponent(new SimpleImage(
                            image,
                            true
                    ));
                    powerup.addComponent(new CircleCollider(
                            List.of(WorldSchema.POWERUP_LEVEL),
                            properties.getGamePropertyDouble("gameObjects.invinciblePower.radius")
                    ));
                    scrollable.addChild(powerup);
                    break;
                }
                case "ENEMY_BOSS": {
                    SpriteBuilder boss = new SpriteBuilder(new Boss(
                            new Point(x, y),
                            properties.getGamePropertyDouble("gameObjects.enemyBoss.health")
                    ));
                    Image image = new Image(properties.getGameProperty("gameObjects.enemyBoss.image"));
                    boss.addComponent(new SimpleImage(
                            image,
                            true
                    ));
                    double radius = properties.getGamePropertyDouble("gameObjects.enemyBoss.activationRadius");

                    double playerRadius = properties.getGamePropertyDouble("gameObjects.player.radius");
                    // collider checks for circle rectangle intersection
                    // so we need to subtract the player radius from the 500px
                    // for it to intersect at 500px
                    double adjustedLength = 2 * (radius - playerRadius);
                    // just subtract the player radius
                    boss.addComponent(new RectCollider(
                            List.of(WorldSchema.BOSS_LEVEL),
                            // for both the left and right needs to be distance 500
                            adjustedLength,
                            Double.POSITIVE_INFINITY
                    ));
                    boss.addComponent(new CircleCollider(
                            List.of(WorldSchema.FIREBALL_LEVEL),
                            properties.getGamePropertyDouble("gameObjects.enemyBoss.radius")
                    ));
                    boss.setName(BOSS);
                    scrollable.addChild(boss);
                    break;
                }
                default: {
                    System.err.println("warning, unknown entity type!");
                }
            }
        }

        return world.build();
    }
}
