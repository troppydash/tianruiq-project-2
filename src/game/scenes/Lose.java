package game.scenes;

import bagel.Font;
import bagel.Image;
import bagel.Input;
import bagel.Keys;
import bagel.util.Point;
import engine.Scene;
import engine.Sprite;
import engine.SpriteBuilder;
import engine.components.image.SimpleImage;
import engine.components.text.MultilineText;
import property.ProgramProperty;

import java.util.Optional;

/**
 * Losing scene
 */
public class Lose extends Scene {
    /**
     * Load the scene
     */
    @Override
    public void load() {
        ProgramProperty properties = ProgramProperty.getInstance();

        // create background
        SpriteBuilder background = new SpriteBuilder();
        background.addComponent(new SimpleImage(new Image(properties.getGameProperty("backgroundImage"))));
        this.instantiateImmediate(background.build());

        // create text
        String font = properties.getGameProperty("font");
        SpriteBuilder text = new SpriteBuilder(new Sprite(new Point(
                (double) properties.getGamePropertyInt("windowWidth") / 2,
                properties.getGamePropertyInt("message.y")
        )));
        String instruction = properties.getMessageProperty("gameOver");
        int instructionSize = properties.getGamePropertyInt("message.fontSize");
        Font instructionFont = new Font(font, instructionSize);
        text.addComponent(new MultilineText(instructionFont, instructionSize, instruction, true));
        this.instantiateImmediate(text.build());
    }

    /**
     * Scene input handle
     *
     * @param input Window input
     * @return Optional scene to change to
     */
    @Override
    public Optional<Scene> update(Input input) {
        if (input.isDown(Keys.SPACE)) {
            this.changeScene(new Start());
        }

        return super.update(input);
    }
}
