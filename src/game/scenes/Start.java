package game.scenes;

import bagel.Image;
import bagel.Input;
import bagel.Keys;
import bagel.util.Point;
import bagel.Font;
import engine.Scene;
import engine.Sprite;
import engine.SpriteBuilder;
import engine.components.text.MultilineText;
import engine.components.image.SimpleImage;
import engine.components.text.SimpleText;
import property.ProgramProperty;

import java.util.Optional;

/**
 * The game start scene
 */
public class Start extends Scene {
    /**
     * Load the start scene
     */
    @Override
    public void load() {
        ProgramProperty properties = ProgramProperty.getInstance();

        String font = properties.getGameProperty("font");

        // create the background
        SpriteBuilder background = new SpriteBuilder();
        background.addComponent(new SimpleImage(new Image(properties.getGameProperty("backgroundImage"))));
        this.instantiateImmediate(background.build());

        // create title
        SpriteBuilder title = new SpriteBuilder(new Sprite(new Point(
                properties.getGamePropertyInt("title.x"),
                properties.getGamePropertyInt("title.y")
        )));
        int titleSize = properties.getGamePropertyInt("title.fontSize");
        Font titleFont = new Font(font, titleSize);
        title.addComponent(new SimpleText(titleFont, titleSize, properties.getMessageProperty("title")));
        this.instantiateImmediate(title.build());


        // create instructions
        SpriteBuilder instructions = new SpriteBuilder(new Sprite(new Point(
                (double) properties.getGamePropertyInt("windowWidth") / 2,
                properties.getGamePropertyInt("instruction.y")
        )));
        String instruction = properties.getMessageProperty("instruction");
        int instructionSize = properties.getGamePropertyInt("instruction.fontSize");
        Font instructionFont = new Font(font, instructionSize);
        instructions.addComponent(new MultilineText(instructionFont, instructionSize, instruction, true));
        this.instantiateImmediate(instructions.build());
    }

    /**
     * Game level select input handle
     * @param input Window input
     * @return New scene
     */
    @Override
    public Optional<Scene> update(Input input) {
        // change scene if number is pressed
        if (input.wasPressed(Keys.NUM_1)) {
            this.changeScene(new Game(Level.LEVEL_1));
        }

        if (input.wasPressed(Keys.NUM_2)) {
            this.changeScene(new Game(Level.LEVEL_2));
        }

        if (input.wasPressed(Keys.NUM_3)) {
            this.changeScene(new Game(Level.LEVEL_3));
        }


        return super.update(input);
    }
}
