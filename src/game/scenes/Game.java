package game.scenes;

import bagel.Font;
import bagel.Image;
import bagel.util.Colour;
import bagel.util.Point;
import engine.Scene;
import engine.Sprite;
import engine.SpriteBuilder;
import engine.components.text.PropertyFormatter;
import engine.components.text.PropertyText;
import engine.components.image.SimpleImage;
import game.WorldSchema;
import game.entities.Boss;
import game.entities.Player;
import property.ProgramProperty;


import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Gameplay scene
 */
public class Game extends Scene {
    private Level level;

    /**
     * Create a game scene on a given level
     * @param level Game level
     */
    public Game(Level level) {
        super();

        this.level = level;
    }


    /**
     * Load the game and level
     */
    @Override
    public void load() {
        ProgramProperty properties = ProgramProperty.getInstance();

        // create the background
        SpriteBuilder background = new SpriteBuilder();
        background.addComponent(new SimpleImage(new Image(properties.getGameProperty("backgroundImage"))));
        this.instantiateImmediate(background.build());

        // create world
        try (Scanner scanner = new Scanner(new FileReader(this.level.getLevelFile()))) {
            Sprite world = WorldSchema.buildWorld(scanner);
            this.instantiateImmediate(world);
        } catch (IOException err) {
            System.err.println("failed to read world file");
            err.printStackTrace(System.err);
            return;
        }

        // add score ui
        Font scoreFont = new Font(
                properties.getGameProperty("font"),
                properties.getGamePropertyInt("score.fontSize")
        );
        SpriteBuilder scoreText = new SpriteBuilder(new Sprite(new Point(
                properties.getGamePropertyDouble("score.x"),
                properties.getGamePropertyDouble("score.y")
        )));
        scoreText.addComponent(new PropertyText<Integer>(
                scoreFont,
                properties.getMessageProperty("score")
        ));
        scoreText.setName(Player.SCORE_TEXT);
        this.instantiateImmediate(scoreText.build());

        // add health ui
        Font healthFont = new Font(
                properties.getGameProperty("font"),
                properties.getGamePropertyInt("playerHealth.fontSize")
        );
        SpriteBuilder healthText = new SpriteBuilder(new Sprite(new Point(
                properties.getGamePropertyDouble("playerHealth.x"),
                properties.getGamePropertyDouble("playerHealth.y")
        )));
        healthText.addComponent(new PropertyText<>(
                healthFont,
                properties.getMessageProperty("health"),
                PropertyFormatter.PERCENT
        ));
        healthText.setName(Player.HEALTH_TEXT);
        this.instantiateImmediate(healthText.build());


        // add boss health ui if exists
        Boss boss = this.findSpriteByName(Boss.class, WorldSchema.BOSS);
        if (boss != null) {
            Font bossFont = new Font(
                    properties.getGameProperty("font"),
                    properties.getGamePropertyInt("enemyBossHealth.fontSize")
            );
            SpriteBuilder bossText = new SpriteBuilder(new Sprite(new Point(
               properties.getGamePropertyDouble("enemyBossHealth.x"),
               properties.getGamePropertyDouble("enemyBossHealth.y")
            )));
            bossText.setName(Boss.HEALTH_TEXT);

            PropertyText<Double> text = new PropertyText<>(
                    bossFont,
                    properties.getMessageProperty("health"),
                    PropertyFormatter.PERCENT
            );
            text.setColor(Colour.RED);
            bossText.addComponent(text);

            this.instantiateImmediate(bossText.build());
        }
    }
}

