package game.scenes;

import property.ProgramProperty;

/**
 * The levels of the game
 */
public enum Level {
    LEVEL_1,
    LEVEL_2,
    LEVEL_3;

    /**
     * Gets the level file names for this level
     *
     * @return Level file path
     */
    public String getLevelFile() {
        ProgramProperty property = ProgramProperty.getInstance();
        switch (this) {
            case LEVEL_1: {
                return property.getGameProperty("level1File");
            }
            case LEVEL_2: {
                return property.getGameProperty("level2File");
            }
            case LEVEL_3: {
                return property.getGameProperty("level3File");
            }
        }

        throw new RuntimeException("invalid level file type");
    }
}
