package game.entities;

import bagel.Input;
import bagel.Keys;
import bagel.util.Point;
import bagel.util.Vector2;
import engine.Scene;
import engine.Sprite;
import engine.components.image.DynamicImage;
import engine.components.colliders.Collision;
import engine.components.text.PropertyText;
import game.WorldSchema;
import game.scenes.Lose;
import game.scenes.Win;

import java.util.HashMap;
import java.util.Map;


/**
 * Entity representing the player
 */
public class Player extends Sprite {
    /**
     * image index of player left
     */
    public static final int PLAYER_RIGHT = 1;
    /**
     * image index of player right
     */
    public static final int PLAYER_LEFT = 0;
    /**
     * death falling speed
     */
    public static final double DIE_SPEED = 2;
    /**
     * gravity speed
     */
    public static final double GRAVITY = 1;
    /**
     * jump initial velocity
     */
    public static final double JUMP_SPEED = 20 + GRAVITY;  // because we subtract GRAVITY in the first frame
    /**
     * reference to score ui text
     */
    public static final String SCORE_TEXT = "SCORE_TEXT";
    /**
     * reference to health ui text
     */
    public static final String HEALTH_TEXT = "HEALTH_TEXT";

    private final double radius;
    private double maxHealth;
    private double health;
    private int score;

    /**
     * player movement speed
     */
    private double speed;

    /**
     * player vertical speed
     */
    private double vspeed;

    /**
     * whether the player is grounded
     */
    private boolean isGrounded;

    /**
     * if the last platform player is on is a flying platform
     */
    private FlyingPlatform lastFlying;

    /**
     * map of powerups
     */
    private Map<PowerType, Integer> powerups;

    /**
     * if the player can attack the boss
     */
    private boolean canAttack;

    private DynamicImage image;

    /**
     * Creates a player with local position, radius, health and speed
     *
     * @param offset Local position
     * @param radius Player radius
     * @param health Player max health
     * @param speed  Player horizontal speed
     */
    public Player(
            Point offset,
            double radius,
            double health,
            double speed
    ) {
        super(offset);

        this.image = null;

        this.radius = radius;
        this.health = health;
        this.maxHealth = health;
        this.score = 0;

        this.speed = speed;
        this.vspeed = 0;
        this.isGrounded = true;
        this.lastFlying = null;

        this.powerups = new HashMap<>();

        this.canAttack = false;
    }


    /**
     * Sprite start logic
     */
    @Override
    public void start() {
        this.image = this.getComponent(DynamicImage.class);
        this.image.swap(PLAYER_RIGHT);

        super.start();
    }

    /**
     * Initiate a jump if allowed
     */
    private void startJump() {
        // ignore if airborne
        if (!this.isGrounded) {
            return;
        }

        // set jump velocity
        this.vspeed = -JUMP_SPEED;
        this.isGrounded = false;
    }

    /**
     * Starts the falling of the player, used when falling off a cliff
     */
    private void startFall() {
        this.vspeed = 0;
        this.isGrounded = false;
    }

    /**
     * Stops the falling
     *
     * @param surfaceLevel The surface y level to snap the player to
     */
    private void stopFallSurface(double surfaceLevel) {
        this.vspeed = 0;
        this.isGrounded = true;
        this.setLocal(new Point(
                this.getLocal().x,
                surfaceLevel - this.radius  // offset for the center is player center
        ));
    }

    /**
     * Stops the falling, but snap to the position y
     *
     * @param position Y level to snap to
     */
    private void stopFallPosition(double position) {
        this.vspeed = 0;
        this.isGrounded = true;
        this.setLocal(new Point(
                this.getLocal().x,
                position
        ));
    }

    /**
     * Returns if the flying platform is landable
     *
     * @param platform New platform
     * @return If the new platform is landable
     */
    private boolean isFlyingPlatformLandable(FlyingPlatform platform) {
        // always landable if last platform is not flying
        if (this.lastFlying == null) {
            return true;
        }

        // landable only if this platform is equal or higher than last one
        return platform.getPositionLevel() <= this.lastFlying.getPositionLevel();
    }

    private void addScore(int extra) {
        if (this.hasPowerup(PowerType.DOUBLE_POINTS)) {
            extra *= 2;
        }
        this.score += extra;
    }

    /**
     * Deal damage to the player
     *
     * @param damage The amount of damage
     */
    private void inflictDamage(double damage) {
        this.health -= damage;
        this.health = Math.max(0, this.health);
    }

    /**
     * Checks whether the player is dead
     *
     * @return If the player is dead
     */
    public boolean isDead() {
        return Double.compare(this.health, 0) <= 0;
    }

    private void updateUI() {
        Sprite scoreText = this.getOwner().findSpriteByName(Sprite.class, SCORE_TEXT);
        Sprite healthText = this.getOwner().findSpriteByName(Sprite.class, HEALTH_TEXT);

        scoreText.getComponent(PropertyText.class).updateProperty(this.score);
        healthText.getComponent(PropertyText.class).updateProperty(this.health / this.maxHealth);
    }

    private boolean hasPowerup(PowerType type) {
        return this.powerups.containsKey(type);
    }

    private void addPowerup(Powerup powerup) {
        this.powerups.put(powerup.collect(), powerup.getDuration());
    }

    private void tickPowerup() {
        // update all powerups
        Map<PowerType, Integer> newPowerups = new HashMap<>();
        for (PowerType power : this.powerups.keySet()) {
            // only keep the power ups with longer than 1 second time
            if (this.powerups.get(power) > 1) {
                newPowerups.put(power, this.powerups.get(power) - 1);
            }
        }
        this.powerups = newPowerups;
    }

    /**
     * Shoots a fireball
     */
    private void attack() {
        Boss boss = this.getOwner().findSpriteByName(Boss.class, WorldSchema.BOSS);
        // can't attack if boss doesn't exist or is dead
        if (boss == null || boss.isDead()) {
            return;
        }

        // fire in the direction
        Vector2 direction = Vector2.right;
        if (this.image.getActiveImage() == PLAYER_LEFT) {
            direction = Vector2.left;
        }

        // compute the scrollable local position of the player
        Sprite scrollable = this.getOwner().findSpriteByName(Sprite.class, WorldSchema.SCROLLABLE);
        Point local = new Point(
                this.getPosition().x - scrollable.getPosition().x,
                this.getPosition().y
        );
        Fireball fireball = Fireball.create(local, this, direction);

        // add to scrollable
        this.getOwner().instantiate(fireball, scrollable);
    }

    /**
     * Collision start logic
     * @param collision Collision
     */
    @Override
    public void beginCollide(Collision collision) {
        // if colliding with the boss area collider
        if (collision.level == WorldSchema.BOSS_LEVEL && collision.target.getOwner() instanceof Boss) {
            this.canAttack = true;
        }
    }

    /**
     * Collision during logic
     * @param collision Collision
     */
    @Override
    public void onCollide(Collision collision) {
        Sprite other = collision.target.getOwner();
        if (other instanceof Coin) {
            // handle coin
            Coin coin = (Coin) other;
            if (!coin.isCollected()) {
                this.addScore(coin.collectCoin());
            }

        } else if (other instanceof Enemy) {
            // handle enemy collision
            Enemy enemy = (Enemy) other;
            if (!enemy.isInflicted() && !this.hasPowerup(PowerType.INVINCIBLE)) {
                this.inflictDamage(enemy.inflictDamage());
            }

        } else if (other instanceof Platform) {
            // platform collision
            Platform platform = (Platform) other;
            if (!this.isGrounded) {
                this.stopFallSurface(platform.getSurfaceLevel());
            }
            this.lastFlying = null;

        } else if (other instanceof FlyingPlatform) {
            // flying platform collision only if the last platform is not flying
            FlyingPlatform platform = (FlyingPlatform) other;
            if (this.isFlyingPlatformLandable(platform)) {
                if (!isGrounded) {
                    this.stopFallPosition(platform.getPositionLevel());
                }
                this.lastFlying = platform;
            }

        } else if (other instanceof Flag) {
            // the boss must not exist or is dead to win
            Boss boss = this.getOwner().findSpriteByName(Boss.class, WorldSchema.BOSS);
            if (boss == null || boss.isDead()) {
                this.getOwner().changeScene(new Win());
            }

        } else if (other instanceof Powerup) {
            Powerup powerup = (Powerup) other;
            if (!powerup.getCollected()) {
                this.addPowerup(powerup);
            }

        } else if (other instanceof Fireball) {
            Fireball fireball = (Fireball) other;
            if (fireball.isActive()
                    && fireball.getBy() != this
                    && !this.hasPowerup(PowerType.INVINCIBLE)) {
                // damage player
                this.inflictDamage(fireball.getDamage());
            }
        }
    }

    /**
     * Collision end logic
     * @param collision Collision
     */
    @Override
    public void leaveCollide(Collision collision) {
        Sprite other = collision.target.getOwner();
        if (collision.level == WorldSchema.SCREEN_LEVEL) {
            // kill player if player falling out of screen
            this.getOwner().changeScene(new Lose());

        } else if (other instanceof Platform || other instanceof FlyingPlatform) {
            // fall if left the platform and is currently grounded
            if (this.isGrounded) {
                this.startFall();
            }

        }

        // if not colliding with the boss area collider
        if (collision.level == WorldSchema.BOSS_LEVEL && other instanceof Boss) {
            this.canAttack = false;
        }
    }

    /**
     * Player update logic
     * @param input Window input
     */
    @Override
    public void update(Input input) {
        if (!this.isDead()) {
            Sprite scrollable = this.getOwner().findSpriteByName(Sprite.class, WorldSchema.SCROLLABLE);

            // process movement
            if (input.isDown(Keys.LEFT)) {
                scrollable.setLocal(
                        new Point(
                                Math.min(0, scrollable.getLocal().x + this.speed),
                                scrollable.getLocal().y
                        )
                );
                this.image.swap(Player.PLAYER_LEFT);
            }
            if (input.isDown(Keys.RIGHT)) {
                scrollable.setLocal(
                        scrollable.getLocal().asVector().add(Vector2.left.mul(this.speed)).asPoint()
                );
                this.image.swap(Player.PLAYER_RIGHT);
            }
            // jump
            if (input.wasPressed(Keys.UP)) {
                this.startJump();
            }
            // attack
            if (input.wasPressed(Keys.S)) {
                if (this.canAttack) {
                    this.attack();
                }
            }
        }

        // apply velocity & gravity only when airborne and alive
        if (!this.isGrounded && !this.isDead()) {
            this.vspeed += GRAVITY;
            this.setLocal(new Point(
                    this.getLocal().x,
                    this.getLocal().y + this.vspeed
            ));
        }

        // a constant velocity when dead
        if (this.isDead()) {
            this.setLocal(new Point(
                    this.getLocal().x,
                    this.getLocal().y + DIE_SPEED
            ));
        }
        super.update(input);

        this.tickPowerup();
        this.updateUI();
    }
}
