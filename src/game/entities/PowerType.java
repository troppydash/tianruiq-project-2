package game.entities;

/**
 * Types of powerups
 */
public enum PowerType {
    DOUBLE_POINTS,
    INVINCIBLE,
}
