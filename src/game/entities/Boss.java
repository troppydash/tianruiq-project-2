package game.entities;

import bagel.Input;
import bagel.util.Point;
import bagel.util.Vector2;
import engine.Sprite;
import engine.components.colliders.Collision;
import engine.components.text.PropertyText;
import game.WorldSchema;

import java.util.concurrent.ThreadLocalRandom;

/**
 * The boss entity
 */
public class Boss extends Sprite {
    /**
     * Boss health ui reference
     */
    public static final String HEALTH_TEXT = "BOSS_HEALTH_TEXT";
    /**
     * Delay to shoot a fireball
     */
    public static final int FIREBALL_DELAY = 100;
    /**
     * Death falling speed
     */
    public static final int DIE_SPEED = 2;

    private double health;
    private boolean playerInRange;
    private int nextAttack;

    /**
     * Creates the boss with position and max health
     *
     * @param position Local position
     * @param health   Max health
     */
    public Boss(Point position, double health) {
        super(position);

        this.health = health;
        this.playerInRange = false;
        this.nextAttack = FIREBALL_DELAY;
    }

    /**
     * If the boss is dead
     *
     * @return If the boss is dead
     */
    public boolean isDead() {
        return Double.compare(this.health, 0) <= 0;
    }

    private void inflictDamage(double damage) {
        this.health -= damage;
        this.health = Math.max(0, this.health);
    }

    private void tickAttack() {
        this.nextAttack -= 1;
        if (this.nextAttack <= 0) {
            this.nextAttack = FIREBALL_DELAY;
            if (this.playerInRange && ThreadLocalRandom.current().nextBoolean()) {
                this.attackPlayer();
            }
        }
    }

    private void attackPlayer() {
        // check that a player exists
        Player player = this.getOwner().findSpriteByName(Player.class, WorldSchema.PLAYER);
        if (player == null || player.isDead()) {
            return;
        }

        Vector2 direction;
        if (player.getPosition().x < this.getPosition().x) {
            direction = Vector2.left;
        } else {
            direction = Vector2.right;
        }

        Fireball fireball = Fireball.create(this.getLocal(), this, direction);
        // add to scrollable
        this.getOwner().instantiate(fireball, this.getOwner().findSpriteByName(Sprite.class, WorldSchema.SCROLLABLE));
    }

    /**
     * Collision start logic
     * @param collision Collision
     */
    @Override
    public void beginCollide(Collision collision) {
        if (collision.target.getOwner() instanceof Player) {
            this.playerInRange = true;
        }
    }

    /**
     * Collision end logic
     * @param collision Collision
     */
    @Override
    public void leaveCollide(Collision collision) {
        if (collision.target.getOwner() instanceof Player) {
            this.playerInRange = false;
        }
    }

    /**
     * Collision during logic
     * @param collision Collision
     */
    @Override
    public void onCollide(Collision collision) {
        Sprite other = collision.target.getOwner();
        if (collision.level == WorldSchema.FIREBALL_LEVEL && other instanceof Fireball && !this.isDead()) {
            Fireball fireball = (Fireball) other;
            if (fireball.isActive()
                    && fireball.getBy() != this) {
                // take damage when an active fireball not by self is hit
                this.inflictDamage(fireball.getDamage());
            }
        }
    }

    private void updateUI() {
        Sprite health = this.getOwner().findSpriteByName(Sprite.class, HEALTH_TEXT);
        if (health == null) {
            return;
        }

        PropertyText component = health.getComponent(PropertyText.class);
        if (component == null) {
            return;
        }

        component.updateProperty(this.health);
    }

    /**
     * Handle updates
     * @param input Window input
     */
    @Override
    public void update(Input input) {
        if (!this.isDead()) {
            // attack when not dead
            this.tickAttack();
        } else {
            // die
            this.setLocal(new Point(
                    this.getLocal().x,
                    this.getLocal().y + DIE_SPEED
            ));
        }

        super.update(input);

        this.updateUI();
    }
}
