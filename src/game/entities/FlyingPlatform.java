package game.entities;

import bagel.util.Point;
import engine.Sprite;
import engine.components.colliders.PlatformCollider;

/**
 * A flying platform
 */
public class FlyingPlatform extends Sprite {
    /**
     * Create flying platform with position
     *
     * @param position Local position
     */
    public FlyingPlatform(Point position) {
        super(position);
    }

    /**
     * Computes the y position where the player should snap to
     *
     * @return The y position
     */
    public double getPositionLevel() {
        // we use getPosition here because this is used for physics outside
        PlatformCollider collider = this.getComponent(PlatformCollider.class);
        return this.getPosition().y - collider.height;
    }
}
