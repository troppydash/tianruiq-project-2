package game.entities;

import bagel.Input;
import bagel.util.Point;
import engine.Sprite;

/**
 * Represents all types of powerups
 */
public class Powerup extends Sprite {
    /**
     * the speed at which the powerup floats up
     */
    public static final double FLOAT_SPEED = 10;

    private PowerType powerType;
    private boolean collected;
    private int duration;

    /**
     * Creates a powerup entity with local, type, and duration
     *
     * @param location  Local position
     * @param powerType Powerup type
     * @param duration  Powerup duration
     */
    public Powerup(Point location, PowerType powerType, int duration) {
        super(location);
        this.powerType = powerType;
        this.duration = duration;
        this.collected = false;
    }

    /**
     * Collect the powerup and collect itself
     *
     * @return The powerup type
     */
    public PowerType collect() {
        if (this.collected) {
            return null;
        }

        this.collected = true;
        return this.powerType;
    }

    /**
     * Gets the duration of this powerup
     *
     * @return Powerup duration
     */
    public int getDuration() {
        return this.duration;
    }

    /**
     * If the powerup is already collected
     *
     * @return If the powerup is already collected
     */
    public boolean getCollected() {
        return this.collected;
    }

    /**
     * Powerup update logic
     * @param input Window input
     */
    @Override
    public void update(Input input) {
        if (this.collected) {
            this.setLocal(new Point(
                    this.getLocal().x,
                    this.getLocal().y - FLOAT_SPEED
            ));
        }

        super.update(input);
    }
}
