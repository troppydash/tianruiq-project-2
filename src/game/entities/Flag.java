package game.entities;

import bagel.Image;
import bagel.Input;
import bagel.util.Point;
import engine.Sprite;

/**
 * Winning game flag
 */
public class Flag extends Sprite {
    /**
     * Create flag with position
     *
     * @param offset Local position
     */
    public Flag(Point offset) {
        super(offset);
    }
}
