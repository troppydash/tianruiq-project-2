package game.entities;

import bagel.Input;
import bagel.util.Point;
import engine.Sprite;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Enemy blob
 */
public class Enemy extends Sprite {
    private boolean inflicted;
    private double damage;

    /**
     * Create enemy with position and damage
     *
     * @param offset Local position
     * @param damage Damage to others
     */
    public Enemy(Point offset, double damage) {
        super(offset);

        this.inflicted = false;
        this.damage = damage;
    }


    /**
     * Return the damage dealt and deactivate the enemy
     *
     * @return Damage dealt
     */
    public double inflictDamage() {
        this.inflicted = true;
        return this.damage;
    }

    /**
     * Whether the enemy has inflicted damage already
     *
     * @return Has already inflicted damage
     */
    public boolean isInflicted() {
        return inflicted;
    }
}
