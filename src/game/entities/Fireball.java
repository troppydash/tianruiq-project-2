package game.entities;

import bagel.Image;
import bagel.Input;
import bagel.util.Point;
import bagel.util.Vector2;
import engine.Sprite;
import engine.SpriteBuilder;
import engine.components.colliders.CircleCollider;
import engine.components.image.SimpleImage;
import game.WorldSchema;
import property.ProgramProperty;

import java.util.List;

/**
 * A fireable fireball
 */
public class Fireball extends Sprite {
    private double damage;
    private Point velocity;
    private Sprite by;
    private boolean active;

    /**
     * Creates a fireball
     *
     * @param position Local position
     * @param damage   Fireball damage
     * @param velocity Vector velocity
     * @param owner    Sprite who fired the ball
     */
    public Fireball(Point position, double damage, Point velocity, Sprite owner) {
        super(position);

        this.damage = damage;
        this.velocity = velocity;
        this.by = owner;
        this.active = true;
    }

    /**
     * Create a new fireball with owner and direction
     *
     * @param local     The local position
     * @param sprite    The owner
     * @param direction The direction of travel
     * @return Fireball class to be instantiated into the scene
     */
    public static Fireball create(Point local, Sprite sprite, Vector2 direction) {
        ProgramProperty property = ProgramProperty.getInstance();

        double speed = property.getGamePropertyDouble("gameObjects.fireball.speed");
        SpriteBuilder fireball = new SpriteBuilder(new Fireball(
                local,
                property.getGamePropertyDouble("gameObjects.fireball.damageSize"),
                direction.mul(speed).asPoint(),
                sprite
        ));
        fireball.addComponent(new SimpleImage(
                new Image(property.getGameProperty("gameObjects.fireball.image")),
                true
        ));
        fireball.addComponent(new CircleCollider(
                List.of(WorldSchema.FIREBALL_LEVEL),
                property.getGamePropertyDouble("gameObjects.fireball.radius")
        ));
        return (Fireball) fireball.build();
    }

    /**
     * Gets the sprite who fired the ball
     *
     * @return The sprite who fired the ball
     */
    public Sprite getBy() {
        return by;
    }

    /**
     * If the fireball is active
     *
     * @return If the fireball can deal damage
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Returns the damage of the fireball then removes itself
     *
     * @return The fireball damage
     */
    public double getDamage() {
        if (!this.active) {
            return 0;
        }

        // remove itself and damage the player
        this.getOwner().remove(this);
        this.active = false;
        return damage;
    }

    /**
     * Fireball update logic
     * @param input Window input
     */
    @Override
    public void update(Input input) {
        // move the ball
        this.setLocal(new Point(
                this.getLocal().x + this.velocity.x,
                this.getLocal().y + this.velocity.y
        ));

        super.update(input);
    }
}
