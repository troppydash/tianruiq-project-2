package game.entities;

import bagel.Image;
import bagel.Input;
import bagel.util.Point;
import engine.Sprite;

/**
 * A collectable coin
 */
public class Coin extends Sprite {
    /**
     * the speed at which the coin floats up
     */
    public static final double FLOAT_SPEED = 10;
    private int value;
    private boolean collected;

    /**
     * Creates a coin with position and value
     *
     * @param offset Local position
     * @param value  Initial value
     */
    public Coin(Point offset, int value) {
        super(offset);

        this.value = value;
        this.collected = false;
    }

    /**
     * Whether the coin has been collected
     *
     * @return Has been collected
     */
    public boolean isCollected() {
        return this.collected;
    }

    /**
     * Returns the coin value and set the coin as collected
     *
     * @return Coin value
     */
    public int collectCoin() {
        this.collected = true;
        return this.value;
    }

    /**
     * Handle update
     * @param input Window input
     */
    @Override
    public void update(Input input) {
        // float up when collected
        if (this.collected) {
            this.setLocal(new Point(
                    this.getLocal().x,
                    this.getLocal().y - FLOAT_SPEED
            ));
        }

        super.update(input);
    }
}
