package game.entities;

import bagel.Image;
import bagel.Input;
import bagel.util.Point;
import engine.Sprite;
import engine.components.colliders.RectCollider;

/**
 * Entity representing the platform
 */
public class Platform extends Sprite {

    /**
     * Creates a platform with position
     *
     * @param offset Local position
     */
    public Platform(Point offset) {
        super(offset);
    }

    /**
     * Returns the surface level of the platform
     *
     * @return The surface y level
     */
    public double getSurfaceLevel() {
        // we use getPosition here because this is used for physics outside
        RectCollider collider = this.getComponent(RectCollider.class);
        return this.getPosition().y - collider.height / 2;
    }
}
