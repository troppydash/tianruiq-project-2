package engine;

import bagel.Input;
import engine.components.colliders.Collider;
import engine.components.colliders.Collision;

import java.util.*;

/**
 * Dictates a sprite addition operation with an optional parent
 */
class Addition {
    /**
     * Sprite to add
     */
    public final Sprite sprite;
    /**
     * the parent to attach the sprite, null implies the scene root
     */
    public final Sprite parent;

    /**
     * Creates an addition
     *
     * @param sprite Sprite to add
     * @param parent Parent to attach the sprite
     */
    public Addition(Sprite sprite, Sprite parent) {
        this.sprite = sprite;
        this.parent = parent;
    }

    /**
     * Creates an addition to the scene root
     *
     * @param sprite Sprite to add
     */
    public Addition(Sprite sprite) {
        this.sprite = sprite;
        this.parent = null;
    }
}


/**
 * Represents a scene in the game
 */
public abstract class Scene {
    /**
     * root sprite group in the scene
     */
    private Sprite root;

    /**
     * list of colliders for each level
     */
    private Map<Integer, List<Collider>> colliders;

    /**
     * set of collisions in the last frame
     */
    private Set<Collision> lastCollisions;

    /**
     * potential next scene
     */
    private Scene nextScene;

    /**
     * the list of removals for the next update
     */
    private List<Sprite> removals;

    /**
     * the list of sprite additions for the next update
     */
    private List<Addition> additions;

    /**
     * Creates a new scene
     */
    public Scene() {
        this.root = new Sprite();
        this.colliders = new HashMap<>();
        this.lastCollisions = new HashSet<>();
        this.nextScene = null;
        this.removals = new ArrayList<>();
        this.additions = new ArrayList<>();
    }

    /**
     * Initialize the scene
     */
    public void load() {
        // do nothing
    }

    /**
     * Stops the scene
     */
    public void destroy() {
        // do nothing
    }


    /**
     * Prompts a scene change, which is performed at the end of updates
     *
     * @param next New scene
     */
    public void changeScene(Scene next) {
        this.nextScene = next;
    }

    private void processSprite(Sprite sprite) {
        // add colliders
        List<Collider> colliders = sprite.getBaseComponents(Collider.class);
        for (Collider collider : colliders) {
            List<Integer> levels = collider.getLevels();
            for (int level : levels) {
                if (!this.colliders.containsKey(level)) {
                    this.colliders.put(level, new ArrayList<>());
                }
                this.colliders.get(level).add(collider);
            }
        }
    }

    private void computeCollisions() {
        Set<Collision> nextCollisions = new HashSet<>();

        // computes and dispatches collision checking
        for (int level : colliders.keySet()) {
            List<Collider> c = colliders.get(level);
            for (int i = 0; i < c.size(); ++i) {
                for (int j = i + 1; j < c.size(); ++j) {
                    Collider left = c.get(i);
                    Collider right = c.get(j);
                    if (left.doesCollide(right)) {
                        Collision collisionLeft = new Collision(level, left, right);
                        Collision collisionRight = new Collision(level, right, left);
                        Sprite leftOwner = left.getOwner();
                        Sprite rightOwner = right.getOwner();

                        nextCollisions.add(collisionLeft);
                        nextCollisions.add(collisionRight);

                        if (!this.lastCollisions.contains(collisionLeft)) {
                            leftOwner.beginCollide(collisionLeft);
                        }
                        if (!this.lastCollisions.contains(collisionRight)) {
                            rightOwner.beginCollide(collisionRight);
                        }

                        leftOwner.onCollide(collisionLeft);
                        rightOwner.onCollide(collisionRight);
                    }
                }
            }
        }

        // update old collisions
        for (Collision oldCollision : this.lastCollisions) {
            if (!nextCollisions.contains(oldCollision)) {
                oldCollision.base.getOwner().leaveCollide(oldCollision);
            }
        }
        this.lastCollisions = nextCollisions;
    }

    /**
     * handles references in a new addition
     */
    private void processInstantiate(Addition addition) {
        Sprite sprite = addition.sprite;

        // process children
        for (Sprite child : sprite.getChildren()) {
            this.processInstantiate(new Addition(child, sprite));
        }

        // process the sprite
        this.processSprite(sprite);
        sprite.setOwner(this);
        sprite.start();
    }

    /**
     * handles references in a new removal
     */
    private void processRemoval(Sprite sprite) {
        sprite.stop();

        // process children
        for (Sprite child : sprite.getChildren()) {
            this.processRemoval(child);
        }
    }

    /**
     * Adds a sprite to the scene instantly
     *
     * @param sprite The sprite to add
     */
    public void instantiateImmediate(Sprite sprite) {
        this.root.addChild(sprite);
        this.processInstantiate(new Addition(sprite));
    }

    /**
     * Adds a sprite to the scene at the root
     *
     * @param sprite The sprite
     */
    public void instantiate(Sprite sprite) {
        this.additions.add(new Addition(sprite));
    }

    /**
     * Adds a sprite to the scene with a parent
     *
     * @param sprite The sprite
     * @param parent The parent sprite
     */
    public void instantiate(Sprite sprite, Sprite parent) {
        this.additions.add(new Addition(sprite, parent));
    }

    /**
     * Removes a sprite from the scene
     *
     * @param sprite The sprite
     */
    public void remove(Sprite sprite) {
        // do nothing if removing the root sprite
        if (sprite == this.root) {
            return;
        }

        this.removals.add(sprite);
    }

    /**
     * Attempts to find a sprite in the scene
     *
     * @param expected The class type
     * @param name     The sprite name
     * @return The optional sprite
     */
    public <T extends Sprite> T findSpriteByName(Class<T> expected, String name) {
        return this.root.getChildByName(expected, name);
    }


    /**
     * Updates the root group sprite and optionally returns a change scene action
     *
     * @param input Window input
     * @return The new scene class, or empty if no change
     */
    public Optional<Scene> update(Input input) {
        // process additions
        for (Addition addition : this.additions) {
            // add to root or parent
            if (addition.parent == null) {
                this.root.addChild(addition.sprite);
            } else {
                addition.parent.addChild(addition.sprite);
            }

            this.processInstantiate(addition);
        }
        this.additions.clear();

        // process removals
        for (Sprite sprite : this.removals) {
            // removing it from its parent will kill off the entire child tree
            sprite.getParent().removeChild(sprite);
            this.processRemoval(sprite);

        }
        this.removals.clear();

        // update and compute collisions
        this.root.update(input);
        this.computeCollisions();

        // handle next scene
        if (this.nextScene == null) {
            return Optional.empty();
        } else {
            return Optional.of(this.nextScene);
        }
    }

    /**
     * Render the scene
     */
    public void render() {
        this.root.render();
    }
}
