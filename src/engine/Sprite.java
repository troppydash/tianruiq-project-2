package engine;

import bagel.Input;
import bagel.util.Point;
import engine.components.Component;
import engine.components.colliders.Collision;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A sprite in a scene
 */
public class Sprite {
    private static final String DEFAULT_NAME = "unnamed sprite";

    /**
     * unique name of sprite
     */
    private String name;

    /**
     * The child sprites associated with the sprite
     */
    private List<Sprite> children;
    /**
     * The parent sprite, can be null
     */
    private Sprite parent;
    /**
     * the scene that owns the sprite
     */
    private Scene owner;

    /**
     * the sprite's components
     */
    private List<Component> components;

    /**
     * The position of the sprite, as an offset
     */
    private Point local;

    /**
     * Creates an empty sprite at the default position
     */
    public Sprite() {
        this.owner = null;
        this.local = new Point();
        this.children = new ArrayList<>();
        this.components = new ArrayList<>();
        this.parent = null;
        this.name = DEFAULT_NAME;
    }

    /**
     * Creates an empty sprite at position
     *
     * @param local Local position
     */
    public Sprite(Point local) {
        this.owner = null;
        this.local = local;
        this.children = new ArrayList<>();
        this.components = new ArrayList<>();
        this.parent = null;
        this.name = DEFAULT_NAME;
    }

    /**
     * The sprite name
     *
     * @return The sprite name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the sprite name
     *
     * @param name The new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the scene owner
     *
     * @return The owner scene
     */
    public Scene getOwner() {
        return owner;
    }

    /**
     * Sets the owner scene
     *
     * @param owner New owner scene
     */
    public void setOwner(Scene owner) {
        this.owner = owner;
    }

    /**
     * Gets the parent sprite
     *
     * @return The parent sprite
     */
    public Sprite getParent() {
        return parent;
    }

    /**
     * Gets the list of child sprites
     *
     * @return Child sprites
     */
    public List<Sprite> getChildren() {
        return children;
    }

    /**
     * Adds the new sprite as the child of this sprite, only in structure
     *
     * @param sprite The new child
     */
    void addChild(Sprite sprite) {
        sprite.parent = this;
        this.children.add(sprite);
    }

    /**
     * Removes a child of the sprite, only in structure
     *
     * @param child The child to remove
     */
    void removeChild(Sprite child) {
        child.parent = null;
        this.children.remove(child);
    }

    /**
     * Recursively searches for the sprite with name and type
     *
     * @param cls  Sprite type
     * @param name The sprite name
     * @return An optional sprite
     */
    public <T extends Sprite> T getChildByName(Class<T> cls, String name) {
        if (this.name.equals(name) && this.getClass().equals(cls)) {
            return cls.cast(this);
        }

        for (Sprite child : this.children) {
            Sprite result = child.getChildByName(cls, name);
            if (result != null) {
                return cls.cast(result);
            }
        }

        return null;
    }


    /**
     * Attach a component to the class
     *
     * @param component New component
     */
    void addComponent(Component component) {
        component.setOwner(this);
        this.components.add(component);
    }

    /**
     * Gets a component by its exact class
     *
     * @param cls The class type
     * @param <T> Class Type
     * @return The object of type class
     */
    public <T extends Component> T getComponent(Class<T> cls) {
        for (Component component : this.components) {
            if (component.getClass().equals(cls)) {
                return cls.cast(component);
            }
        }

        return null;
    }

    /**
     * Gets a list of components with the same base class
     *
     * @param cls Base class
     * @param <T> Base class type
     * @return List of components
     */
    public <T extends Component> List<T> getBaseComponents(Class<T> cls) {
        return this.components
                .stream()
                .filter(cls::isInstance)
                .map(cls::cast)
                .collect(Collectors.toList());
    }

    /**
     * Returns the world position of the sprite
     *
     * @return The world position
     */
    public Point getPosition() {
        if (this.parent == null) {
            return this.local;
        }

        return (this.parent.getPosition().asVector().add(this.local.asVector())).asPoint();
    }

    /**
     * The local position of the sprite
     *
     * @return The local position
     */
    public Point getLocal() {
        return this.local;
    }

    /**
     * Sets the local position of the sprite
     *
     * @param local The new local position
     */
    public void setLocal(Point local) {
        this.local = local;
    }

    /**
     * When the sprite first enters the scene
     */
    public void start() {
        for (Component component : this.components) {
            component.start();
        }
    }

    /**
     * When the sprite is removed from the scene
     */
    public void stop() {
        for (Component component : this.components) {
            component.stop();
        }
    }

    /**
     * Updates itself and all children
     * @param input Window input
     */
    public void update(Input input) {
        // update children first
        for (Sprite child : this.children) {
            child.update(input);
        }

        // then components
        for (Component component : this.components) {
            if (component.isActive()) {
                component.update(input);
            }
        }
    }

    /**
     * Renders itself and all children
     */
    public void render() {
        // render the children first
        for (Sprite child : this.children) {
            child.render();
        }

        // then components
        for (Component component : this.components) {
            if (component.isActive()) {
                component.render();
            }
        }
    }

    /**
     * Tell the sprite that a collision is happening
     */
    public void onCollide(Collision collision) {
        // nothing here, need to be overridden
    }

    /**
     * Tell the sprite that a collision occurred
     */
    public void beginCollide(Collision collision) {
        // nothing here, need to be overridden
    }

    /**
     * Tell the sprite that a collision just finished
     */
    public void leaveCollide(Collision collision) {
        // nothing here, need to be overridden
    }

}
