package engine.components.image;

import bagel.Image;
import bagel.util.Point;
import engine.Sprite;
import engine.components.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * A dynamic sprite with multiple switchable images
 */
public class DynamicImage extends Component {
    /**
     * list of images
     */
    private List<Image> images;
    /**
     * currently active image, -1 if no images are loaded
     */
    private int active;

    /**
     * Creates an empty dynamic image
     */
    public DynamicImage() {
        super();
        this.images = new ArrayList<>();
        this.active = -1;
    }

    /**
     * Creates a pre-populated dynamic image
     *
     * @param images Images
     */
    public DynamicImage(List<Image> images) {
        super();
        this.images = images;
        this.active = -1;
    }

    /**
     * Gets the active image index
     *
     * @return Active image index
     */
    public int getActiveImage() {
        return this.active;
    }

    /**
     * Append a new image
     *
     * @param newImage The new image
     */
    public void pushImage(Image newImage) {
        this.images.add(newImage);
    }

    /**
     * Swaps to the last image in the list
     */
    public void swap() {
        if (this.images.isEmpty()) {
            this.active = -1;
            return;
        }

        this.active = this.images.size() - 1;
    }

    /**
     * Swaps to a specific image in the list
     *
     * @param index The image index
     */
    public void swap(int index) {
        if (index < 0 || index >= this.images.size()) {
            return;
        }

        this.active = index;
    }

    /**
     * Image render
     */
    @Override
    public void render() {
        // render image if the active image is valid
        if (this.active >= 0 && this.active < this.images.size()) {
            Point position = this.getOwner().getPosition();
            this.images.get(this.active).draw(position.x, position.y);
        }
    }
}
