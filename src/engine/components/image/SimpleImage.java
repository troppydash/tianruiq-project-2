package engine.components.image;

import bagel.Image;
import bagel.util.Point;
import engine.Sprite;
import engine.components.Component;

/**
 * A simple image sprite implementation, position as from top left or center
 */
public class SimpleImage extends Component {
    /**
     * image file
     */
    private final Image image;
    private boolean centered;

    /**
     * Creates a top-left anchored image with an image file
     *
     * @param image Image file
     */
    public SimpleImage(Image image) {
        this.image = image;
    }

    /**
     * Creates a centering image
     *
     * @param image    Image file
     * @param centered Centering
     */
    public SimpleImage(Image image, boolean centered) {
        this.image = image;
        this.centered = centered;
    }

    /**
     * Image render
     */
    @Override
    public void render() {
        // draw the image, with the position as top left
        Point position = this.getOwner().getPosition();

        if (this.centered) {
            this.image.draw(position.x, position.y);
        } else {
            this.image.drawFromTopLeft(position.x, position.y);
        }
    }
}
