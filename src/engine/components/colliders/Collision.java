package engine.components.colliders;

import engine.Sprite;
import engine.components.colliders.Collider;

import java.util.Objects;

/**
 * Represents a collision between two colliders
 */
public class Collision {
    /**
     * base collider, onCollide always has base to be the owner sprite
     */
    public final Collider base;
    /**
     * target collider, the other collider
     */
    public final Collider target;
    /**
     * level for the collision
     */
    public final int level;

    /**
     * Create a collision in level with a base and target collider
     *
     * @param level  Collision level
     * @param base   Base collider
     * @param target Target collider
     */
    public Collision(int level, Collider base, Collider target) {
        this.level = level;
        this.base = base;
        this.target = target;
    }

    /**
     * Returns whether the collision has a target sprite
     *
     * @param cls Target sprite class
     * @return If the target sprite is the type
     */
    public <T extends Sprite> boolean collidedWith(Class<T> cls) {
        return this.target.getOwner().getClass().equals(cls);
    }

    /**
     * Equality override
     * @param o Other object
     * @return If the two collisions are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Collision collision = (Collision) o;
        return level == collision.level && Objects.equals(base, collision.base) && Objects.equals(target, collision.target);
    }

    /**
     * Hashing operator
     * @return The hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(base, target, level);
    }
}
