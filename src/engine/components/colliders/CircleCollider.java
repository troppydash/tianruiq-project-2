package engine.components.colliders;

import java.util.List;

/**
 * A circular collider
 */
public class CircleCollider extends Collider {
    /**
     * the collider radius
     */
    public final double radius;

    /**
     * Creates a circle collider with the collision levels and radius
     *
     * @param level  Collision levels
     * @param radius Circle radius
     */
    public CircleCollider(List<Integer> level, double radius) {
        super(level);
        this.radius = radius;
    }

    /**
     * Collision logic
     * @param other The other collider
     * @return If there is a collision
     */
    @Override
    public boolean doesCollide(Collider other) {
        // circle v circle collision
        if (other.getClass().equals(CircleCollider.class)) {
            CircleCollider circle = (CircleCollider) other;

            double distance = this.getPosition().distanceTo(other.getPosition());
            return distance <= circle.radius + this.radius;
        }

        // circle v rect collision
        if (other instanceof RectCollider) {
            return other.doesCollide(this);
        }

        if (other instanceof PlatformCollider) {
            return other.doesCollide(this);
        }

        return false;
    }
}
