package engine.components.colliders;

import bagel.util.Point;

import java.util.List;

/**
 * A rectangular collider
 */
public class RectCollider extends Collider {
    /**
     * width of collider
     */
    public final double height;
    /**
     * height of collider
     */
    public final double width;

    /**
     * Create a rect collider at level with platform width and height
     *
     * @param level  Collider level
     * @param width  Platform width
     * @param height Platform height
     */
    public RectCollider(List<Integer> level, double width, double height) {
        super(level);
        this.width = width;
        this.height = height;
    }

    /**
     * Returns if the point is in a rectangular boundary
     *
     * @param left   Boundary left
     * @param right  Boundary right
     * @param top    Boundary top
     * @param bottom Boundary bottom
     * @param x      Point x
     * @param y      Point y
     * @return whether the point is in the boundary
     */
    private boolean isPointInBoundary(double left, double right, double top, double bottom, double x, double y) {
        return x <= right && x >= left && y >= top && y <= bottom;
    }

    private boolean computeRectCircle(CircleCollider other) {
        Point position = other.getPosition();
        double radius = other.radius;

        Point rectPosition = this.getPosition();
        double width = this.width;
        double height = this.height;
        double left = rectPosition.x - width / 2;
        double right = rectPosition.x + width / 2;
        double top = rectPosition.y - height / 2;
        double bottom = rectPosition.y + height / 2;

        // check if circle center within the rect boundary + radius
        if (!this.isPointInBoundary(
                left - radius, right + radius,
                top - radius, bottom + radius,
                position.x, position.y
        )) {
            // if not, it is not colliding
            return false;
        }

        // if any circle boundary is in the rectangle, then we must be colliding
        if (
                this.isPointInBoundary(
                        left, right, top, bottom,
                        position.x - radius, position.y
                ) || this.isPointInBoundary(
                        left, right, top, bottom,
                        position.x + radius, position.y
                ) || this.isPointInBoundary(
                        left, right, top, bottom,
                        position.x, position.y - radius
                ) || this.isPointInBoundary(
                        left, right, top, bottom,
                        position.x, position.y + radius
                )
        ) {
            return true;
        }

        // the corners may not be handled, so check if the corners are in the circle
        Point topleft = new Point(left, top);
        Point topright = new Point(right, top);
        Point bottomleft = new Point(left, bottom);
        Point bottomright = new Point(right, bottom);

        if (position.distanceTo(topright) <= radius
                || position.distanceTo(topleft) <= radius
                || position.distanceTo(bottomleft) <= radius
                || position.distanceTo(bottomright) <= radius
        ) {
            // at least one corner is in the circle, a collision
            return true;
        }

        // else we have an edge case
        return false;
    }


    /**
     * Collision method
     * @param other The other collider
     * @return If there is a collision
     */
    @Override
    public boolean doesCollide(Collider other) {
        if (other.getClass().equals(CircleCollider.class)) {
            // handle rect to circle collision
            return this.computeRectCircle((CircleCollider) other);
        }

        return false;
    }
}
