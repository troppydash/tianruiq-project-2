package engine.components.colliders;

import bagel.util.Point;

import java.util.List;

/**
 * A collider for the flying platforms
 */
public class PlatformCollider extends Collider {
    /**
     * collider height padding
     */
    public static final int PADDING = 1;

    /**
     * width of the platform
     */
    public final double width;
    /**
     * height offset of the collider
     */
    public final double height;

    /**
     * Create a platform collider with level, width of platform, and height offset
     *
     * @param level  Collider level
     * @param width  Width of platform
     * @param height Height offset
     */
    public PlatformCollider(List<Integer> level, double width, double height) {
        super(level);
        this.width = width;
        this.height = height;
    }

    /**
     * Returns if the point is in a rectangular boundary
     *
     * @param left   Boundary left
     * @param right  Boundary right
     * @param top    Boundary top
     * @param bottom Boundary bottom
     * @param x      Point x
     * @param y      Point y
     * @return whether the point is in the boundary
     */
    private boolean isPointInBoundary(double left, double right, double top, double bottom, double x, double y) {
        return x <= right && x >= left && y >= top && y <= bottom;
    }

    /**
     * computes if the other collider's center is in the rectangle, or passed it
     */
    private boolean computeRectPoint(Collider other) {
        Point position = other.getPosition();
        Point lastPosition = other.getLastPosition();

        Point rectPosition = this.getPosition();

        double width = this.width;
        double height = this.height;
        double left = rectPosition.x - width / 2;
        double right = rectPosition.x + width / 2;
        double top = rectPosition.y - height;
        double bottom = rectPosition.y - height + PADDING;

        // check if the object passed through it
        if (position.x >= left && position.x <= right) {
            if (lastPosition.y < bottom && position.y > top) {
                return true;
            }
            if (lastPosition.y > top && position.y < bottom) {
                return true;
            }
        }

        // check if circle center within the rect boundary
        return this.isPointInBoundary(
                left, right,
                top, bottom,
                position.x, position.y
        );
    }

    /**
     * Collision logic
     * @param other The other collider
     * @return If there is a collision
     */
    @Override
    public boolean doesCollide(Collider other) {
        return this.computeRectPoint(other);
    }
}
