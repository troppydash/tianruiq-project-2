package engine.components.colliders;

import bagel.Input;
import bagel.util.Point;
import engine.components.Component;

import java.util.List;

/**
 * Base collider class for all collider types
 */
public abstract class Collider extends Component {
    /**
     * the local center of the collider
     */
    private Point local;

    /**
     * last collider position
     */
    private Point lastPosition;
    /**
     * current collider position
     */
    private Point currentPosition;

    /**
     * colliders only affect each other on the same level
     */
    private List<Integer> levels;

    /**
     * Create collider on the levels
     *
     * @param levels Collision levels
     */
    public Collider(List<Integer> levels) {
        this.local = new Point();
        this.lastPosition = new Point();
        this.currentPosition = new Point();
        this.levels = levels;
    }

    /**
     * Create collider on the levels with an initial position
     *
     * @param position Local position
     * @param levels   Collision levels
     */
    public Collider(Point position, List<Integer> levels) {
        this.local = position;
        this.lastPosition = position;
        this.currentPosition = position;
        this.levels = levels;
    }

    /**
     * Get the collision levels of the collider
     *
     * @return Collision levels
     */
    public List<Integer> getLevels() {
        return this.levels;
    }

    /**
     * Computes the world position for the collider, used to compute collisions
     *
     * @return The world position
     */
    public Point getPosition() {
        if (this.owner == null) {
            return this.local;
        }

        return this.owner.getPosition().asVector().add(this.local.asVector()).asPoint();
    }

    /**
     * The last frame collider world position
     *
     * @return The last frame collider world position
     */
    public Point getLastPosition() {
        return this.lastPosition;
    }


    /**
     * Checks if the collider collides with another
     *
     * @param other The other collider
     * @return If they collide
     */
    public abstract boolean doesCollide(Collider other);

    /**
     * Collider update
     * @param input Window input
     */
    @Override
    public void update(Input input) {
        // updates last position and current position
        // note that this update is called BEFORE the collision handling in scene
        this.lastPosition = this.currentPosition;
        this.currentPosition = this.getPosition();
    }
}
