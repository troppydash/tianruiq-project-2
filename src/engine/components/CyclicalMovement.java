package engine.components;

import bagel.Input;
import bagel.util.Point;
import engine.components.Component;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Allows the sprite to move along a straight line, in a cyclical pattern
 */
public class CyclicalMovement extends Component {
    private double maxDisplacement;
    private boolean movingLeft;
    private double movingSpeed;
    private double targetDisplacement;

    /**
     * Creates the movement component with a maximum displacement and horizontal speed
     *
     * @param maxDisplacement Maximum horizontal displacement
     * @param speed           Horizontal speed
     */
    public CyclicalMovement(double maxDisplacement, double speed) {
        super();

        this.maxDisplacement = maxDisplacement;
        this.movingSpeed = speed;
        this.movingLeft = false;
        this.targetDisplacement = 0;
    }

    /**
     * Start logic
     */
    @Override
    public void start() {
        this.chooseDirection(true);
    }

    /**
     * chooses a direction to move to, either randomly or inverting the current direction
     */
    private void chooseDirection(boolean random) {
        // randomly choose a direction or invert the previous direction
        if (random) {
            this.movingLeft = ThreadLocalRandom.current().nextBoolean();
        } else {
            this.movingLeft = !this.movingLeft;
        }

        // set the new target
        double x = this.owner.getLocal().x;
        if (this.movingLeft) {
            this.targetDisplacement = -this.maxDisplacement + x;
        } else {
            this.targetDisplacement = this.maxDisplacement + x;
        }
    }

    /**
     * Component update logic
     * @param input Window input
     */
    @Override
    public void update(Input input) {
        if (!this.active) {
            return;
        }

        double x = this.owner.getLocal().x;
        double y = this.owner.getLocal().y;
        if (Math.abs(x - this.targetDisplacement) < this.movingSpeed) {
            // snap movement if we overreach
            this.owner.setLocal(new Point(this.targetDisplacement, y));
            this.chooseDirection(false);
        } else {
            // else move by speed
            if (this.movingLeft) {
                this.owner.setLocal(new Point(
                        x - this.movingSpeed,
                        y
                ));
            } else {
                this.owner.setLocal(new Point(
                        x + this.movingSpeed,
                        y
                ));
            }
        }
    }
}
