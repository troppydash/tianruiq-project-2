package engine.components.text;

import bagel.Font;
import bagel.util.Point;
import engine.components.Component;

/**
 * To render multiline text with specific line spacings
 */
public class MultilineText extends Component {
    private final Font font;
    private int lineSpacing;
    private String text;
    private boolean centered;

    /**
     * Create a multiline text with font, line spacings, text, and centering
     *
     * @param font        Font
     * @param lineSpacing Vertical line spacing
     * @param text        Text content
     * @param centered    If the text is centered
     */
    public MultilineText(Font font, int lineSpacing, String text, boolean centered) {
        super();
        this.font = font;
        this.lineSpacing = lineSpacing;
        this.text = text;
        this.centered = centered;
    }

    /**
     * Create a multiline text with font, line spacings, text
     *
     * @param font        Font
     * @param lineSpacing Vertical line spacing
     * @param text        Text content
     */
    public MultilineText(Font font, int lineSpacing, String text) {
        this(font, lineSpacing, text, false);
    }

    /**
     * Updates the text content
     *
     * @param newText New text content
     */
    public void updateText(String newText) {
        this.text = newText;
    }

    private void renderLine(String line, int yoffset) {
        if (this.centered) {
            // render the text centered
            double width = this.font.getWidth(line);
            Point position = this.getOwner().getPosition();
            this.font.drawString(line, position.x - width / 2, position.y + yoffset);
        } else {
            // render the text anchored bottom left
            Point position = this.getOwner().getPosition();
            this.font.drawString(line, position.x, position.y + yoffset);
        }
    }

    /**
     * Text render
     */
    @Override
    public void render() {
        String[] lines = this.text.split("\n");

        for (int i = 0; i < lines.length; ++i) {
            this.renderLine(lines[i], i * this.lineSpacing);
        }
    }
}
