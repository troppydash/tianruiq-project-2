package engine.components.text;

import bagel.DrawOptions;
import bagel.Font;
import bagel.util.Colour;
import bagel.util.Point;
import engine.components.Component;

/**
 * Simple property text renderer
 */
public class PropertyText<V> extends Component {
    private final Font font;
    private String prefix;
    private V value;
    private boolean centered;
    private PropertyFormatter<V> formatable;
    private Colour color;

    /**
     * Creates a property text
     *
     * @param font       Font
     * @param prefix     Property name
     * @param centered   Centering
     * @param formatable Formatter
     */
    public PropertyText(Font font, String prefix, boolean centered, PropertyFormatter<V> formatable) {
        super();
        this.font = font;
        this.prefix = prefix;
        this.centered = centered;
        this.formatable = formatable;
        this.color = Colour.WHITE;
    }

    /**
     * Creates a property text
     *
     * @param font       Font
     * @param text       Property name
     * @param formatable Formatter
     */
    public PropertyText(Font font, String text, PropertyFormatter<V> formatable) {
        this(font, text, false, formatable);
    }

    /**
     * Creates a default property text
     *
     * @param font Font
     * @param text Property name
     */
    public PropertyText(Font font, String text) {
        // use the default interface
        this(font, text, false, Object::toString);
    }

    /**
     * Updates the property name
     *
     * @param newText New property name
     */
    public void updatePrefix(String newText) {
        this.prefix = newText;
    }

    /**
     * Updates the property value
     *
     * @param property New property value
     */
    public void updateProperty(V property) {
        this.value = property;
    }

    /**
     * Updates the property color
     *
     * @param color New property color
     */
    public void setColor(Colour color) {
        this.color = color;
    }

    private DrawOptions createDrawOptions() {
        DrawOptions options = new DrawOptions();
        options.setBlendColour(this.color);
        return options;
    }

    /**
     * Text render
     */
    @Override
    public void render() {
        if (this.value == null) {
            return;
        }

        String text = this.prefix + this.formatable.format(this.value);
        if (this.centered) {
            // render the text centered
            double width = this.font.getWidth(text);
            Point position = this.getOwner().getPosition();
            this.font.drawString(text, position.x - width / 2, position.y, this.createDrawOptions());
        } else {
            // render the text anchored bottom left
            Point position = this.getOwner().getPosition();
            this.font.drawString(text, position.x, position.y, this.createDrawOptions());
        }
    }
}
