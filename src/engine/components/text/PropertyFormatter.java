package engine.components.text;

/**
 * Format a property into a string
 *
 * @param <V> The property type
 */
public interface PropertyFormatter<V> {
    /**
     * Formats a property into a string representation
     *
     * @param value Property value
     * @return String representations
     */
    String format(V value);

    /**
     * Defualt Formatter
     */
    PropertyFormatter<Object> DEFAULT = String::valueOf;
    /**
     * Percent Formatter
     */
    PropertyFormatter<Double> PERCENT = value -> String.valueOf(Math.round(value * 100));
}
