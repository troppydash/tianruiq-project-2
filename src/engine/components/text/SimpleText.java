package engine.components.text;

import bagel.Font;
import bagel.util.Point;
import engine.components.Component;

/**
 * Simple text renderer
 */
public class SimpleText extends Component {
    private final Font font;
    private String text;
    private int size;
    private boolean centered;

    /**
     * Creates a simple piece of text
     *
     * @param font     Font
     * @param size     Font size
     * @param text     Text content
     * @param centered Centering
     */
    public SimpleText(Font font, int size, String text, boolean centered) {
        super();
        this.font = font;
        this.size = size;
        this.text = text;
        this.centered = centered;
    }

    /**
     * Creates a simple left-centered text
     *
     * @param font Font
     * @param size Font size
     * @param text Text content
     */
    public SimpleText(Font font, int size, String text) {
        this(font, size, text, false);
    }

    /**
     * Sets the text content
     *
     * @param newText New text content
     */
    public void setText(String newText) {
        this.text = newText;
    }

    /**
     * Text rendering
     */
    @Override
    public void render() {
        if (this.centered) {
            // render the text centered
            double width = this.font.getWidth(this.text);
            Point position = this.getOwner().getPosition();
            this.font.drawString(this.text, position.x - width / 2, position.y);
        } else {
            // render the text anchored bottom left
            Point position = this.getOwner().getPosition();
            this.font.drawString(this.text, position.x, position.y);
        }
    }
}
