package engine.components;

import bagel.Input;
import bagel.util.Point;
import engine.Sprite;


/**
 * An attachable component to the sprite class
 */
public abstract class Component {
    /**
     * if the component should receive render/update calls
     */
    protected boolean active;
    /**
     * parent sprite owner
     */
    protected Sprite owner;

    /**
     * Creates a detached component
     */
    public Component() {
        this.active = false;
        this.owner = null;
    }

    /**
     * Creates an attached component with an owner sprite
     *
     * @param owner Owner sprite
     */
    public Component(Sprite owner) {
        this.active = true;
        this.owner = owner;
    }

    /**
     * If the component is active
     *
     * @return If the component is active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the activeness of the component
     *
     * @param active New active state
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Gets the sprite owner
     *
     * @return Sprite owner
     */
    public Sprite getOwner() {
        return owner;
    }

    /**
     * Sets the sprite owner
     *
     * @param owner New sprite owner
     */
    public void setOwner(Sprite owner) {
        this.owner = owner;
        this.active = true;
    }

    /**
     * Start method called after owner start
     */
    public void start() {
        // optional method that can be overridden
    }

    /**
     * Stop method called before owner stop
     */
    public void stop() {
        // optional method that can be overridden
    }

    /**
     * Renders the component
     */
    public void render() {
        // optional method that can be overridden
    }

    /**
     * Updates the component
     *
     * @param input Window input
     */
    public void update(Input input) {
        // optional method that can be overridden
    }
}
