package engine;

import engine.components.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder for the sprite that exposes the component and child methods
 */
public class SpriteBuilder {
    private Sprite sprite;
    private List<SpriteBuilder> children;
    private List<Component> components;

    /**
     * Creates a builder with a custom sprite
     *
     * @param sprite The custom sprite
     */
    public SpriteBuilder(Sprite sprite) {
        this.sprite = sprite;
        this.children = new ArrayList<>();
        this.components = new ArrayList<>();
    }

    /**
     * Creates a builder with an empty sprite
     */
    public SpriteBuilder() {
        this.sprite = new Sprite();
        this.children = new ArrayList<>();
        this.components = new ArrayList<>();
    }

    /**
     * Sets the sprite name
     *
     * @param name Sprite name
     * @return this SpriteBuilder
     */
    public SpriteBuilder setName(String name) {
        this.sprite.setName(name);
        return this;
    }

    /**
     * Adds a child to the sprite
     *
     * @param child Child builder
     * @return this SpriteBuilder
     */
    public SpriteBuilder addChild(SpriteBuilder child) {
        this.children.add(child);
        return this;
    }

    /**
     * Adds a component to the sprite
     *
     * @param component Component
     * @return this SpriteBuilder
     */
    public SpriteBuilder addComponent(Component component) {
        this.components.add(component);
        return this;
    }

    /**
     * Create and returns the full child
     *
     * @return The complete child object
     */
    public Sprite build() {
        for (SpriteBuilder child : this.children) {
            sprite.addChild(child.build());
        }

        for (Component component : this.components) {
            sprite.addComponent(component);
        }

        return sprite;
    }

}
