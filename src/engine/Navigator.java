package engine;

import bagel.Input;

import java.util.Optional;

/**
 * Encapsulates the scene and their interactions
 */
public class Navigator {
    /**
     * the currently active scene
     */
    private Scene active;

    /**
     * Creates a navigator with a first scene
     *
     * @param initial The first scene
     */
    public Navigator(Scene initial) {
        this.active = null;
        this.loadScene(initial);
    }

    /**
     * Load a new scene by disposing the old scene and starting the new scene
     *
     * @param newScene The new scene
     */
    private void loadScene(Scene newScene) {
        if (this.active != null) {
            this.active.destroy();
        }
        this.active = newScene;
        this.active.load();
    }

    /**
     * Updates the active scene
     *
     * @param input User input
     */
    public void update(Input input) {
        if (this.active == null) {
            return;
        }

        Optional<Scene> scene = this.active.update(input);
        // change scene if asked to
        if (scene.isPresent()) {
            this.loadScene(scene.get());
        }
    }

    /**
     * Renders the active scene
     */
    public void render() {
        if (this.active == null) {
            return;
        }
        this.active.render();
    }
}
