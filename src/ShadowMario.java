import bagel.*;
import engine.Navigator;
import property.ProgramProperty;
import game.scenes.Start;

import java.util.Properties;

/**
 * Skeleton Code for SWEN20003 Project 2, Semester 1, 2024
 * <p>
 * Please enter your name below
 *
 * @author Tianrui Qi
 */
public class ShadowMario extends AbstractGame {

    private Navigator navigator;

    /**
     * The constructor
     */
    public ShadowMario(Properties game_props, Properties message_props) {
        super(Integer.parseInt(game_props.getProperty("windowWidth")),
                Integer.parseInt(game_props.getProperty("windowHeight")),
                message_props.getProperty("title"));

        // load the start scene
        this.navigator = new Navigator(new Start());
    }

    /**
     * Performs a state update.
     * Allows the game to exit when the escape key is pressed.
     */
    @Override
    protected void update(Input input) {
        // close window
        if (input.wasPressed(Keys.ESCAPE)) {
            Window.close();
        }

        // update
        this.navigator.update(input);
        // then render
        this.navigator.render();
    }


    /**
     * The entry point for the program.
     */
    public static void main(String[] args) {
        // pass program properties to the main entry
        ProgramProperty properties = ProgramProperty.getInstance();
        ShadowMario game = new ShadowMario(properties.gameProperties, properties.messageProperties);
        game.run();
    }
}
